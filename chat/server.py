import socket
import socks
from queue import Queue
from time import sleep
from threading import Thread

#s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
host = '127.0.0.1'
port = 5001
ThreadCount = 0
q = Queue()
q.put("Welcome")
clients = []
SOCKS_PORT = 9050
hoster = ["korrupt"]
hostee = ["korrupt"]
password = ["r00t"]
    
class BC:
    G = '\033[92m'
    A = '\033[0;37m'
    F = '\033[91m'
    B = '\033[1m'
    E = '\033[0m'
    
    
def tor():
    global s
    #socks.set_default_proxy(socks.SOCKS5, "127.0.0.1",SOCKS_PORT)
    socket.socket = socks.socksocket
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    #s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.bind(('0.0.0.0', port))
    s.listen(500)
    try:
        s.bind((host, port))
    except socket.error as e:
        print(str(e))
    s.listen(5)

def clearnet():
    global s
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.bind(('0.0.0.0', port))
    s.listen(500)
    try:
        s.bind((host, port))
    except socket.error as e:
        print(str(e))
    s.listen(5)

tor()
print('> Locked And Loaded.')
print('> Waiting For Connections...')
data = "" 

def sending():
    global q
    global clients
    dat = q.get()
    for i in clients:
        i.send(dat)
        print("Sent to " + str(i))
ping = []     
pingn = []    
kill = []  
named = []
def threaded_user(connection):
    global data
    global kill
    global clients
    global ping
    global pingn

    global password
    name = connection.recv(2048).decode('utf-8')
    #namez = "@" + name
    chck = False
    if name in hostee:
        connection.send("Password".encode('utf-8'))
        br = connection.recv(2048).decode('utf-8')
        print(br)
        #else:
        #    connection.send("Password".encode('utf-8'))
    if name in named:
        err = "Error: " + name + " is already in use! Try again."
        connection.send(err.encode('utf-8'))
        clients.remove(connection)
        connection.close()
    else:
        named.append(name)
        conn = "User " + BC.F + name + BC.E + " has connected."
        q.put(conn.encode('utf-8'))
        sending()
    try:
        while True:
            sleep(.5)
            data = connection.recv(2048)
            br = data.decode('utf-8')
            #reply = name + ': ' + data.decode('utf-8')
            if name in kill:
                if name in hoster:
                    pass
                else:
                    connection.send("You have been kicked!".encode('utf-8'))
                    clients.remove(connection)
                    connection.close()
                    named.remove(name)
                    kill.remove(name)
                
            if not data:
                clients.remove(connection)
                dat = 'User ' + BC.F + name + BC.E + " left."
                q.put(dat.encode('utf-8'))
                print(dat)
                named.remove(name)
                sending()
            elif br == "exit":
                lol = 'User ' + BC.F + name + BC.E + " left."
                print(lol)
                q.put(lol.encode('utf-8'))
                clients.remove(connection)
                sending()
            if br[:8] == "Password":
                passed = br[9:]
                print(passed)
                if passed in password:
                    chck = True
                    hostee.remove(name)
                    cont = "User Accepted."
                    connection.send(cont.encode('utf-8'))
                elif passed not in password:
                    clients.remove(connection)
                    connection.close()
                    named.remove(name)
                    kill.remove(name)
            elif br == "!list":
                users = "Current Users: " + BC.F + str(named) + BC.E
                print(users)
                connection.send(users.encode('utf-8'))
                #sending()
            elif br[:1] == "@":
                ping.append(br)
                a = br.split()[0]
                print(a[1:])
                pingn.append(a[1:])
            elif name in pingn:
                q.put(data)
                print(name + ": " + data.decode('utf-8'))
                sending()
                print(ping)
                print(pingn)
                connection.send(ping[0].encode('utf-8'))
                ping = []
                pingn = []
                #sending()
            elif br[:5] == "!kick":
                print(kill)
                if name in hoster:
                    kill.append(br[6:])
                    named.remove(br[6:])
            elif br[:4] == "!add":
                if name in hoster:
                    hoster.append(br[5:])
                    hosters = "Admins: " + str(hoster)
                    connection.send(hosters.encode('utf-8'))
                else:
                    nf = "User: You are not admin. Type !admins to find one.\n"
                    connection.send(nf.encode('utf-8'))
                    nf = "User " + name + " attempted an administrative command."
                    q.put(nf.encode('utf-8'))
                    sending()
            elif br[:7] == "!remove":
                if name in hoster:
                    try:
                        hoster.remove(br[8:])
                        hosters = "Admins: " + str(hoster)
                        connection.send(hosters.encode('utf-8'))
                    except:
                        nou = "Admins: No user named " + br[8:] + " in admins."
                        connection.send(nou.encode('utf-8'))
                else:
                    nf = "User: You are not admin. Type !admins to find one.\n"
                    connection.send(nf.encode('utf-8'))
                    nf = "User " + name + " attempted an administrative command."
                    q.put(nf.encode('utf-8'))
                    sending()
            elif br[:7] == "!admins":
                hosters = "Admins: " + str(hoster)
                connection.send(hosters.encode('utf-8'))
            else:
                q.put(data)
                print(name + ": " + data.decode('utf-8'))
                sending()
    except (ConnectionResetError, BrokenPipeError):
        clients.remove(connection)
        dat = 'User ' + BC.F + name + BC.E + " left."
        q.put(dat.encode('utf-8'))
        sending()
        if name in hoster:
            hostee.append(name)
    except ValueError:
        if name in hoster:
            hostee.append(name)
        dat = 'User ' + BC.F + name + BC.E + " left."
        q.put(dat.encode('utf-8'))
        sending()

Thread(target=sending).start()
while True:
    try:
        Client, address = s.accept()
        clients.append(Client)
        Thread(target = threaded_user, args =(Client,)).start()
        ThreadCount += 1
        print('Thread Number: ' + str(ThreadCount) + " has started.")
    except:
        s.close()